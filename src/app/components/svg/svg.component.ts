import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-svg',
  templateUrl: './svg.component.svg',
  styleUrls: ['./svg.component.scss']
})
export class SvgComponent implements OnInit {
  @Input() text = {
    firstLine: "mejor",
    secondLine: "tasa"
  };
  @Input() fillColors = {
    background: '#FFF34D',
    circle:  '#FFFFFF',
    text: '#212121',  
  }
  
  constructor() { }

  ngOnInit(): void {}
}
